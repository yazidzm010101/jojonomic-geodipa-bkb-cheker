import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'
import styles from '../styles/Home.module.css'
import ErrorPage from 'next/error'
import React from 'react'

export default function Home() {
  const { query: { ref } } = useRouter()
  const [ state, setState ] = React.useState()
  const [ data, setData ] = React.useState()
  
  React.useEffect(() => {
    //case if there is no query params
    if (!ref) {
      setState('not-found')
    } else {
      // loading first
      setState('loading')
      var env = process.env.NEXT_PUBLIC_VERCEL_ENV
      var url = 'https://gateway.geodipa.co.id/v1/nocode/api/mockup/expense/check_bkb_signature'
      if (env == 'production') {
        url = 'https://gateway.geodipa.co.id/v1/nocode/api/expense/check_bkb_signature'
      }      

      // fetch from endpoint
      fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ ref })
      }).then(response => {
        // parse json
        return response.json()
      }).then(data => {
        console.log(data)
        setState('loaded')
        setData(data)
      }).catch(err => {
        console.log(err)
        setState('network-error')
      })
    }
  }, [ref])

  if (state == 'not-found') {
    return <ErrorPage statusCode={404} withDarkMode={false}/>
  }  
  
  if (state == 'network-error') {
    return <ErrorPage statusCode={500} withDarkMode={false}/>
  }  

  if (state == 'loaded' && data.error == true) {
    return <ErrorPage statusCode={400} withDarkMode={false}/>
  }

  if (state == 'loaded' && data.error != true) {
    return (
      <div className='w-full min-h-[100vh] bg-gray-100 text-gray-700 py-12 px-6'>
        <Head><title>{ data.data.request_number }</title></Head>
        <div className='w-full max-w-lg mx-auto'>
          <div className='flex justify-center'>        
            <div className='w-[160px] h-[48px] max-w-full relative'>
              <Image src='/geo_dipa_256.png' layout='fill' objectFit='contain'/>
            </div>
          </div>
          
          <h1 className='leading-loose text-center text-2xl font-bold'>Verifikasi Tanda Tangan Elektronik</h1>
          <p className='leading-loose text-center text-sm italic'>Digital Signature Verification</p>
          <br/>

          <p className='leading-loose text-center text-xl'>PT Geo Dipa Energi (Persero) menyatakan bahwa:</p>
          <p className='leading-loose text-center text-sm italic'>PT Geo Dipa Energi (Persero) states that:</p>
          <br/>

          <div className='bg-white shadow rounded-lg p-4'>            
            <p className='leading-relaxed'>Nama Dokumen:</p>
            <p className='leading-relaxed text-sm italic'>Document Title:</p>
            <p className='leading-relaxed font-bold'>BKB</p>
            <br/>

            <p className='leading-relaxed'>Nomor Dokumen:</p>
            <p className='leading-relaxed text-sm italic'>Document Number:</p>
            <p className='leading-relaxed font-bold'>{ data.data.request_number }</p>
            <br/>

            <p className='leading-relaxed'>Judul Kontrak:</p>
            <p className='leading-relaxed text-sm italic'>Contract Name:</p>
            <p className='leading-relaxed font-bold'>{ data.data.remark }</p>
            <br/>

            <p className='leading-relaxed'>Telah ditandatangani oleh User sebagai berikut:</p>
            <p className='leading-relaxed text-sm italic'>Has been signed by User as follows:</p>                        

            <div className='rounded-lg p-4 bg-gray-200 my-4'>
              <p className='leading-relaxed text-sm'>Nama/Name:</p>
              <p className='leading-relaxed uppercase'>{ data.data.approver }</p>
              <br/>

              <p className='leading-relaxed text-sm'>Posisi/Position:</p>
              <p className='leading-relaxed uppercase'>{ data.data.approver_position }</p>
              <br/>

              <p className='leading-relaxed text-sm'>Waktu/Time:</p>
              <p className='leading-relaxed'>{ new Date(data.data.approved_at).toLocaleDateString('id') + ' ' + new Date(data.data.approved_at).toLocaleTimeString('en-ID', { hourCycle: 'h24' }) }</p>
            </div>          

            <p className='leading-loose text-center text-sm'>
              <div className='w-[16px] h-[16px] inline-block relative align-middle mr-1 mb-1'>
                <Image src='/checklist.png' layout='fill' objectFit='contain'/>
              </div>
              <span className='inline-block align-middle'>Adalah benar dan tercatat pada aplikasi Expense</span>
            </p>
            <p className='leading-loose text-center text-[10px] italic'>That is true and it is recorded in Expense application</p>
            <br/>  

            <p className='leading-loose text-center text-[10px]'>Untuk memastikan kebenaran pernyataan ini pastikan URL pada browser anda adalah https://apps.geodipa.co.id?ref={ref}</p>
            <p className='leading-loose text-center text-[10px] italic'>If you wisth to check the validity of this statement, please ensure the URL of your browser is https://apps.geodipa.co.id?ref={ref}</p>
            <br/>

          </div>

          <div className='flex flex-col items-center justify-center mt-8'>        
            <p className='leading-loose text-center text-[10px]'>Powered by</p>
            <div className='w-[160px] h-[18px] max-w-full relative'>
              <Image src='/oos.png' layout='fill' objectFit='contain'/>
            </div>
          </div>
        </div>        
      </div>
    )
  }

  return <h1>Loading...</h1>  
}
